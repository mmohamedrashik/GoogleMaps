package com.example.alphaone.mapsapps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.security.Provider;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    GoogleMap map;
    UiSettings mapsetings;
    GoogleApiClient googleApiClient;


    // static final LatLng location= new LatLng(0,0);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.fragment)).getMap();
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMyLocationEnabled(true);
        mapsetings = map.getUiSettings();
        mapsetings.setZoomGesturesEnabled(true);
        mapsetings.setZoomControlsEnabled(true);
        mapsetings.setCompassEnabled(true);
        // Marker marker = map.addMarker(new MarkerOptions().position(location).title("CENTRAL"));
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                double a = latLng.latitude;
                double b = latLng.longitude;
                Marker marker = map.addMarker(new MarkerOptions().position(latLng));
                Toast.makeText(getApplication(), a + "  " + b + "", Toast.LENGTH_SHORT).show();

                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(10).build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                double lats = location.getLatitude();
                double longs = location.getLongitude();
                double speed = location.getSpeed();
                LatLng latLng = new LatLng(lats,longs);
                Toast.makeText(getApplication(),lats+" "+longs+" "+speed,Toast.LENGTH_SHORT).show();

                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(10).build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }

        });



        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
